import http
import requests


def test_http_status_ok():
    response = requests.get("https://qalabs.pl")
    assert response.status_code == http.HTTPStatus.OK
